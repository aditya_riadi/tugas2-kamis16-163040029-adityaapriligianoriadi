-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 08, 2018 at 03:06 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `produk_mainan`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` varchar(13) NOT NULL,
  `username` varchar(225) NOT NULL,
  `fullname` varchar(225) NOT NULL,
  `password` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `fullname`, `password`) VALUES
('1', 'aditya', 'Aditya Apriligiano Riadi', '10406c1d7b7421b1a56f0d951e952a95'),
('5c055eb30825a', 'admin', 'ega purnama jaya', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Table structure for table `detail_order`
--

CREATE TABLE `detail_order` (
  `id` int(10) NOT NULL,
  `order_id` int(10) DEFAULT NULL,
  `mainan` int(10) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `harga` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_order`
--

INSERT INTO `detail_order` (`id`, `order_id`, `mainan`, `qty`, `harga`) VALUES
(3, 3, 5, 1, '145000'),
(4, 4, 5, 1, '180000'),
(5, 5, 5, 1, '115000'),
(6, 6, 5, 3, '115000'),
(8, 10, 5, 1, '115000'),
(9, 11, 5, 1, '145000');

-- --------------------------------------------------------

--
-- Table structure for table `mainan`
--

CREATE TABLE `mainan` (
  `id_mainan` varchar(64) NOT NULL,
  `nama_mainan` varchar(100) NOT NULL,
  `harga_mainan` int(11) NOT NULL,
  `stok_mainan` int(9) NOT NULL,
  `deskripsi_mainan` text NOT NULL,
  `img_mainan` varchar(100) NOT NULL DEFAULT 'default.jpg'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mainan`
--

INSERT INTO `mainan` (`id_mainan`, `nama_mainan`, `harga_mainan`, `stok_mainan`, `deskripsi_mainan`, `img_mainan`) VALUES
('5c04083b45a79', 'Action Figure Boruto', 115000, 100, 'Action Figure yang dapat digunakan sebagai Mainan, Pajangan, Miniatur, sangat ideal untuk hadiah ataupun melengkapi koleksi pribadi Anda.', '5c04083b45a79.png'),
('5c0409163c391', 'Action Figure Gara', 150000, 80, 'Action Figure yang dapat digunakan sebagai Mainan, Pajangan, Miniatur, sangat ideal untuk hadiah ataupun melengkapi koleksi pribadi Anda.', '5c0409163c391.png'),
('5c0409377c226', 'Action Figure Goblin Slayer', 145000, 100, 'Action Figure yang dapat digunakan sebagai Mainan, Pajangan, Miniatur, sangat ideal untuk hadiah ataupun melengkapi koleksi pribadi Anda.', '5c0409377c226.png'),
('5c04096098b88', 'Action Figure Gon', 165000, 50, 'Action Figure yang dapat digunakan sebagai Mainan, Pajangan, Miniatur, sangat ideal untuk hadiah ataupun melengkapi koleksi pribadi Anda.', '5c04096098b88.png'),
('5c04097a943c2', 'Action Figure Kaneki', 175000, 100, 'Action Figure yang dapat digunakan sebagai Mainan, Pajangan, Miniatur, sangat ideal untuk hadiah ataupun melengkapi koleksi pribadi Anda.', '5c04097a943c2.png'),
('5c04099d3590e', 'Action Figure Kilua', 180000, 45, 'Action Figure yang dapat digunakan sebagai Mainan, Pajangan, Miniatur, sangat ideal untuk hadiah ataupun melengkapi koleksi pribadi Anda.', '5c04099d3590e.png'),
('5c0409c487762', 'Action Figure Naruto', 200000, 65, 'Action Figure yang dapat digunakan sebagai Mainan, Pajangan, Miniatur, sangat ideal untuk hadiah ataupun melengkapi koleksi pribadi Anda.', '5c0409c487762.png'),
('5c0409e4e358e', 'Action Figure Natsu', 155000, 100, 'Action Figure yang dapat digunakan sebagai Mainan, Pajangan, Miniatur, sangat ideal untuk hadiah ataupun melengkapi koleksi pribadi Anda.', '5c0409e4e358e.png'),
('5c040a1e32798', 'Action Figure One Piece', 230000, 20, 'Action Figure yang dapat digunakan sebagai Mainan, Pajangan, Miniatur, sangat ideal untuk hadiah ataupun melengkapi koleksi pribadi Anda.', '5c040a1e32798.png'),
('5c040a3f629e3', 'Action Figure Rimuru', 189000, 100, 'Action Figure yang dapat digunakan sebagai Mainan, Pajangan, Miniatur, sangat ideal untuk hadiah ataupun melengkapi koleksi pribadi Anda.', '5c040a3f629e3.png'),
('5c040a61afa41', 'Action Figure Saitama', 179000, 55, 'Action Figure yang dapat digunakan sebagai Mainan, Pajangan, Miniatur, sangat ideal untuk hadiah ataupun melengkapi koleksi pribadi Anda.', '5c040a61afa41.png'),
('5c040a837c2f9', 'Action Figure Sakura', 199000, 100, 'Action Figure yang dapat digunakan sebagai Mainan, Pajangan, Miniatur, sangat ideal untuk hadiah ataupun melengkapi koleksi pribadi Anda.', '5c040a837c2f9.png'),
('5c040aa4e5007', 'Action Figure Sasuke', 210000, 10, 'Action Figure yang dapat digunakan sebagai Mainan, Pajangan, Miniatur, sangat ideal untuk hadiah ataupun melengkapi koleksi pribadi Anda.', '5c040aa4e5007.png'),
('5c040abed29d8', 'Action Figure Son Goku', 185000, 80, 'Action Figure yang dapat digunakan sebagai Mainan, Pajangan, Miniatur, sangat ideal untuk hadiah ataupun melengkapi koleksi pribadi Anda.', '5c040abed29d8.png'),
('5c040ad687baa', 'Action Figure Vegeta', 145000, 90, 'Action Figure yang dapat digunakan sebagai Mainan, Pajangan, Miniatur, sangat ideal untuk hadiah ataupun melengkapi koleksi pribadi Anda.', '5c040ad687baa.png');

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `id` int(10) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `pelanggan` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`id`, `tanggal`, `pelanggan`) VALUES
(3, '2018-12-07', 3),
(4, '2018-12-07', 4),
(5, '2018-12-07', 5),
(6, '2018-12-07', 6),
(9, '2018-12-07', 9),
(10, '2018-12-08', 10),
(11, '2018-12-08', 11);

-- --------------------------------------------------------

--
-- Table structure for table `pelanggan`
--

CREATE TABLE `pelanggan` (
  `id` int(10) NOT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `alamat` varchar(50) DEFAULT NULL,
  `telp` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pelanggan`
--

INSERT INTO `pelanggan` (`id`, `nama`, `email`, `alamat`, `telp`) VALUES
(3, 'ega purnama jaya', 'ega@mail.unpas.ac.id', 'majalaya', '081230400290'),
(4, 'pandu w Suralla', 'pandu@mail.unpas.ac.id', 'karawang', '081230400330'),
(5, 'Rifqi Pramansyah', 'rifqi@mail.unpas.ac.id', 'cibiru', '081230400450'),
(6, 'Danu Indra Listanto', 'danu@mail.unpas.ac.id', 'bandung', '089123987234'),
(9, 'wakwaw', 'wakwaw@mail.com', 'pasteur', '123456789'),
(10, 'bayu', 'bayu@mail.com', 'ciwaruga', '081234598765'),
(11, 'ega purnama jaya', 'ega@mail.unpas.ac.id', 'majalaya', '081230400290');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` varchar(13) NOT NULL,
  `fullname` varchar(225) NOT NULL,
  `username` varchar(225) NOT NULL,
  `password` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `fullname`, `username`, `password`) VALUES
('5c05469144225', 'ega purnama jaya', 'ega', 'ada13fdcea33c5eec42ff32e68c021b8'),
('5c0546c581c75', 'Aditya Apriligiano Riadi', 'aditya', '938b4263f09b8b1dae8f027d06681ec9'),
('5c0549c24948a', 'Danu Indra Listanto', 'danu', '938b4263f09b8b1dae8f027d06681ec9'),
('5c0549d96e651', 'Pandu W Suralla', 'pandu', '3271d41b0949eb7a630fd8c184e8f8c1'),
('5c054a294ec54', 'Rifqi Pramansyah', 'rifqi', '0d51943788f22ba4f45d2c7e647f3779');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detail_order`
--
ALTER TABLE `detail_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mainan`
--
ALTER TABLE `mainan`
  ADD PRIMARY KEY (`id_mainan`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detail_order`
--
ALTER TABLE `detail_order`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `pelanggan`
--
ALTER TABLE `pelanggan`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
