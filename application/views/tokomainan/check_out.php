<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Jual Beli Online Mainan</title>

    <!-- Bootstrap core CSS -->
    <link href="<?= base_url() ?>assets/css/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?= base_url() ?>assets/css/shop-homepage.css" rel="stylesheet">

  </head>

  <body>
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="<?= site_url('tokomainan') ?>"><img src="<?= base_url() ?>assets/img/tokoMainan/iconTm.png" alt="" width="80%"></a>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a href="" class="mb-control" data-box="#mb-signout" >
                <i><img src="<?= base_url()  ?>assets/img/tokoMainan/out-sign.png" alt="" width="40%"></i>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Page Content -->
    <div class="container py-2">
      <div class="row">
        <div class="col-lg-12">
            <h2 class="text-center">Konfirmasi Check Out</h2><hr>
            <?php
            $grand_total = 0;
            if ($cart = $this->cart->contents())
                {
                    foreach ($cart as $item)
                        {
                            $grand_total = $grand_total + $item['subtotal'];
                        }    
            ?>
            <form class="form-horizontal" action="<?php echo site_url('tokomainan/proses_order')?>" method="post" name="frmCO" id="frmCO">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group  has-success has-feedback">
                            <label class="control-label col-xs-3" for="inputEmail">Email:</label>
                            <div class="col-xs-9">
                                <input type="email" class="form-control" name="email" id="email" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group  has-success has-feedback">
                            <label class="control-label col-xs-3" for="firstName">Nama :</label>
                            <div class="col-xs-9">
                                <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Lengkap">
                            </div>
                        </div>
                        <div class="form-group  has-success has-feedback">
                            <label class="control-label col-xs-3" for="lastName">Alamat:</label>
                            <div class="col-xs-9">
                                <input type="text" class="form-control" name="alamat" id="alamat" placeholder="Alamat">
                            </div>
                        </div>
                        <div class="form-group  has-success has-feedback">
                            <label class="control-label col-xs-3" for="phoneNumber">Telp:</label>
                            <div class="col-xs-9">
                                <input type="tel" class="form-control" name="telp" id="telp" placeholder="No Telp">
                            </div>
                        </div>
                        
                        <div class="form-group  has-success has-feedback">
                            <div class="col-xs-offset-3 col-xs-9">
                                <button type="submit" class="btn btn-outline-light"><img src="<?= base_url() ?>assets/img/tokomainan/shopping-bag.png" alt=""> Order</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <table class="table">
                            <tr>
                                <td width="2%">No</td>
                                <td width="33%">Item</td>
                                <td width="17%">Harga</td>
                                <td width="8%">Qty</td>
                            </tr>
                        
                        <?php 
                        $i = 1;
                        foreach ($cart as $key) : ?>
                            <tr>
                                <td><?php echo $i++; ?></td>
                                <td><?php echo $key['name']; ?></td>
                                <td><?php echo number_format($key['price'], 0,",","."); ?></td>
                                <td><?php echo $key['qty'] ?></td>
                            </tr>
                        <?php endforeach ?>
                            <tr>
                                <?php 
                                    echo "<h4>Total Belanja: Rp.".number_format($grand_total,0,",",".")."</h4>";
                                 ?>
                            </tr>
                        </table>
                    </div>
                </div>
            </form>
            <?php
            }
            else
                {
                    echo "<div class='alert alert-danger alert-dismissible fade show' role='alert'>
                          <strong>Keranjang Kosong!</strong> Ayo Belanja Terlebih dahulu.
                          <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                            <span aria-hidden='true'>&times;</span>
                          </button>
                        </div>"; 
                }
            ?>
        </div>
        <!-- /.col-lg-12 -->

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- MESSAGE BOX-->
    <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                <div class="mb-content">
                    <p>Are you sure you want to log out?</p>                    
                    <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <a href="<?= site_url('Login/logout') ?>" class="btn btn-success btn-lg">Yes</a>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MESSAGE BOX-->

    <!-- START PRELOADS -->
    <audio id="audio-alert" src="<?= base_url() ?>assets/audio/alert.mp3" preload="auto"></audio>
    <audio id="audio-fail" src="<?= base_url() ?>assets/audio/fail.mp3" preload="auto"></audio>
    <!-- END PRELOADS -->  

    <!-- Bootstrap core JavaScript -->
    <script src="<?= base_url() ?>assets/css/bootstrap/jquery/jquery.js"></script>
    <script src="<?= base_url() ?>assets/css/bootstrap/js/bootstrap.bundle.min.js"></script>
    
    <!-- PLUGIN -->
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/jquery/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/bootstrap/bootstrap.min.js"></script>        
    <!-- END PLUGINS -->                

    <!-- THIS PAGE PLUGINS -->
    <script type='text/javascript' src='<?= base_url() ?>assets/js/plugins/icheck/icheck.min.js'></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
    
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/datatables/jquery.dataTables.min.js"></script>    

    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/bootstrap/bootstrap-datepicker.js"></script>                
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/bootstrap/bootstrap-file-input.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/bootstrap/bootstrap-select.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/tagsinput/jquery.tagsinput.min.js"></script>
    <!-- END PAGE PLUGINS -->

    <!-- START TEMPLATE -->
    
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins.js"></script>        
    <script type="text/javascript" src="<?= base_url() ?>assets/js/actions.js"></script>        
    <!-- END TEMPLATE -->
    <!-- END SCRIPTS --> 

  </body>

</html>