<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title>TM Admin</title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="<?= base_url() ?>assets/css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container page-navigation-top-fixed">
            
            <!-- START PAGE SIDEBAR -->
            <div class="page-sidebar page-sidebar-fixed scroll">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation">
                    <li class="xn-logo">
                        <a href="<?php echo site_url('admin/index') ?>">TM Admin</a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>     
                    <li class="xn-profile">
                        <a href="#" class="profile-mini">
                            <img src="<?= base_url() ?>assets/img/profile/tokoMainan.jpg" alt="Aditya"/>
                        </a>
                        <div class="profile" id="profile">
                            <div class="profile-image">
                                <img src="<?=base_url()  ?>assets/img/profile/tokoMainan.jpg" alt="Aditya"/>
                            </div>
                            <div class="profile-data">
                                <div class="profile-data-name"></div>
                                <div class="profile-data-title">Web Developer/Designer</div>
                            </div>
                            <div class="profile-controls">
                                <a href="#" class="profile-control-left"><span class="fa fa-info"></span></a>
                                <a href="#" class="profile-control-right"><span class="fa fa-envelope"></span></a>
                            </div>
                        </div>                                                                        
                    </li>                                                                           
                    <li class="xn-title">Navigation</li>
                    <li>
                        <a href="<?php echo site_url('admin/index') ?>"><span class="fa fa-desktop"></span> <span class="xn-text">Daftar Mainan</span></a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('admin/tambah_aksi') ?>"><span class="fa fa-plus"></span><span class="xn-text">Mainan</span></a>
                    </li>                                        
                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
                    <!-- SIGN OUT -->
                    <li class="xn-icon-button pull-right">
                        <a href="<?= site_url('Login_Admin/logout') ?>" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>                        
                    </li> 
                    <!-- END SIGN OUT -->
                    <!-- END TOGGLE NAVIGATION -->                    
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->                                    
                
                <div class="page-title"></div>     
