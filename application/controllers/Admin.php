<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Admin extends CI_Controller
{

	public function __construct(){
		parent::__construct();
		$this->load->model('Admin_mainan_model');
		$this->load->library('form_validation');
		if ($this->session->userdata('status') != "login") {
			redirect(site_url('Login_Admin/index'));
		}
	}

	public function index()
	{
		
		$data['mainan']  = $this->Admin_mainan_model->getMainan();
		$data['content'] = 'admin/index';
		$this->load->view('templates/template',$data);
	}

	public function tambah_aksi() {

		$mainan = $this->Admin_mainan_model;
		$validation = $this->form_validation;
		$validation->set_rules($mainan->rules());
		
		if ($validation->run()) {
			$mainan->tambahMainan();
			$this->session->set_flashdata('Success', 'Berhasil disimpan');
		}

		$data['content'] = 'admin/tambah';
		$this->load->view('templates/template', $data);
	}

	public function hapus($id = null){
		if (!isset($id)) show_404();

		if ($this->Admin_mainan_model->hapusMainan($id)) {
			redirect(site_url('admin'));
		}
	}

	public function ubah($id = null){
		if (!isset($id)) redirect('admin/index');
		
		$mainan = $this->Admin_mainan_model;
		$validation = $this->form_validation;
		$validation->set_rules($mainan->rules());

		if ($validation->run() != false) {
			$mainan->ubahMainan($id);
			$this->session->set_flashdata('Success', 'Berhasil disimpan');
		}

		$data['mainan'] = $mainan->getMainanById($id);
		$data['content'] = 'admin/ubah';
		if (!$data['mainan']) show_404();
		$this->load->view('templates/template',$data);
	}

	
}