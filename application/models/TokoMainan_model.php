
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class TokoMainan_model extends CI_Model
{
	private $_table = "mainan";
	public $id_mainan;
	public $nama_mainan;
	public $harga_mainan;
	public $stok_mainan;
	public $deskripsi_mainan;
	public $img_mainan = "default.jpg";
	
	public function getMainan(){
		return $this->db->get($this->_table)->result();
	}

	public function getMainanById($id){
		return $this->db->get_where($this->_table, ['id_mainan'=>$id])->row();
	}

	public function tambah_pelanggan($data)
	{
		$this->db->insert('pelanggan', $data);
		$id = $this->db->insert_id();
		return (isset($id)) ? $id : FALSE;
	}
	
	public function tambah_order($data)
	{
		$this->db->insert('order', $data);
		$id = $this->db->insert_id();
		return (isset($id)) ? $id : FALSE;
	}
	
	public function tambah_detail_order($data)
	{
		$this->db->insert('detail_order', $data);
	}

}